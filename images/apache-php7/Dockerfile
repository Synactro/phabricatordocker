FROM debian:stretch-slim

ENV DEBIAN_FRONTEND noninteractive
ENV TERM xterm

ARG PHP_VERSION="7.3"

RUN apt-get update && apt-get install -y \
    apt-transport-https \
    lsb-release \
    ca-certificates \
    wget \
&& rm -rf /var/lib/apt/lists/*

RUN wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
RUN echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" >> /etc/apt/sources.list.d/php.list

# todo remove --allow-unauthenticated
RUN apt-get update -y && apt-get install -y --allow-unauthenticated \
	git python \
    apache2 php${PHP_VERSION} libapache2-mod-php${PHP_VERSION} \
    curl php${PHP_VERSION}-curl \
    php${PHP_VERSION}-mb php${PHP_VERSION}-gd php${PHP_VERSION}-json php${PHP_VERSION}-apcu \
    php${PHP_VERSION}-mysql mysql-client php${PHP_VERSION}-mysqlnd \
    nano vim ssmtp \
    zip unzip \
&& rm -rf /var/lib/apt/lists/*

RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod expires

ARG LOCAL_USER_ID
ARG LOCAL_GROUP_ID
RUN groupadd groupUser --gid ${LOCAL_GROUP_ID}
RUN useradd user --uid ${LOCAL_USER_ID} --gid groupUser --shell /bin/bash --create-home
RUN adduser www-data groupUser
RUN adduser user www-data

COPY ./etc/php/php.ini /etc/php/${PHP_VERSION}/apache2/conf.d/99-php.ini
COPY ./etc/php/php.ini /etc/php/${PHP_VERSION}/cli/conf.d/99-php.ini

RUN rm /etc/apache2/sites-enabled/*.conf
COPY ./etc/apache/virtualhost /etc/apache2/sites-enabled

COPY ./etc/ssmtp/ssmtp.conf /etc/ssmtp/ssmtp.conf

# Install Pygments
WORKDIR /root
RUN curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
RUN python get-pip.py
RUN pip install Pygments

WORKDIR /var/www

RUN git clone https://github.com/phacility/libphutil.git
RUN git clone https://github.com/phacility/arcanist.git
RUN git clone https://github.com/phacility/phabricator.git

RUN mkdir /var/log/apache/

WORKDIR /var/www/phabricator

COPY ./script/start.sh /root/start.sh
RUN chmod 0700 /root/start.sh

WORKDIR /var/www

ENTRYPOINT ["/root/start.sh"]