#!/bin/bash

cd /var/www/phabricator
./bin/config set mysql.host ${MYSQL_HOST}
./bin/config set mysql.port ${MYSQL_PORT}
./bin/config set mysql.user ${MYSQL_USER}
./bin/config set mysql.pass ${MYSQL_PASSWORD}
./bin/config set phabricator.base-uri ${PHABRICATOR_BASE_URI}
./bin/config set security.alternate-file-domain ${PHABRICATOR_ALTERNATE_FILE_DOMAIN}
./bin/config set storage.local-disk.path /home/user/data/phabricator
./bin/config set phd.user user
./bin/config set pygments.enabled true
./bin/config set storage.mysql-engine.max-size 0

./bin/storage --force upgrade

su user -c "./bin/phd start"

/usr/sbin/apache2ctl -D FOREGROUND