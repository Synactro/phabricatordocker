export PROJECT=synactro-phabricator
export LOCAL_USER_ID=$(shell id -u)
export LOCAL_GROUP_ID=$(shell id -g)

build:
	docker-compose build

build-final:
	LOCAL_USER_ID=1000 LOCAL_GROUP_ID=1000 docker-compose build

start: stop
	docker-compose up -d
	@echo Start dev
	@echo "Web: http://phabricator.local"

stop:
	docker-compose down --remove-orphans

push:
	echo ${DOCKER_PASSWORD} | docker login --username ${DOCKER_USERNAME} --password-stdin ${REGISTRY}

	#version
	docker tag ${PROJECT}/web ${REGISTRY}/${PROJECT}/web:${VERSION}
	docker tag ${PROJECT}/database ${REGISTRY}/${PROJECT}/database:${VERSION}
	docker push ${REGISTRY}/${PROJECT}/web:${VERSION}
	docker push ${REGISTRY}/${PROJECT}/database:${VERSION}

	#short
	docker tag ${PROJECT}/web ${REGISTRY}/${PROJECT}/web:${VERSION_SHORT}
	docker tag ${PROJECT}/database ${REGISTRY}/${PROJECT}/database:${VERSION_SHORT}
	docker push ${REGISTRY}/${PROJECT}/web:${VERSION_SHORT}
	docker push ${REGISTRY}/${PROJECT}/database:${VERSION_SHORT}

	#latest
	docker tag ${PROJECT}/web ${REGISTRY}/${PROJECT}/web:latest
	docker tag ${PROJECT}/database ${REGISTRY}/${PROJECT}/database:latest
	docker push ${REGISTRY}/${PROJECT}/web:latest
	docker push ${REGISTRY}/${PROJECT}/database:latest

clean:
	sudo rm -rf data/mysql/* data/phabricator/*/*

exec-web:
	docker exec -ti --user root PhabricatorWeb bash

exec-database:
	docker exec -ti --user root --workdir /var/lib/mysql/ PhabricatorDatabase bash
